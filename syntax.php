<?php
/**
 * DokuWiki Plugin droolswiki (Syntax Component)
 *
 * @license GPL 2 http://www.gnu.org/licenses/gpl-2.0.html
 * @author  Choma, Lenart <choma.tomasz@gmail.com, mrart@student.agh.edu.pl>
 */

// must be run within Dokuwiki
if (!defined('DOKU_INC')) die();

class syntax_plugin_droolswiki extends DokuWiki_Syntax_Plugin {
    /**
     * @return string Syntax mode type
     */
    public function getType() {
        return 'protected';
    }
    /**
     * @return string Paragraph type
     */
    public function getPType() {
        return 'normal';
    }
    /**
     * @return int Sort order - Low numbers go before high numbers
     */
    public function getSort() {
        return 500;
    }

    /**
     * Connect lookup pattern to lexer.
     *
     * @param string $mode Parser mode
     */
    public function connectTo($mode) {
//        $this->Lexer->addSpecialPattern('<FIXME>',$mode,'plugin_droolswiki');
        $this->Lexer->addEntryPattern('<drools>(?=.*?</drools>)',$mode,'plugin_droolswiki');
    }

    public function postConnect() {
        $this->Lexer->addExitPattern('</drools>','plugin_droolswiki');
    }

    /**
     * Handle matches of the droolswiki syntax
     *
     * @param string $match The match of the syntax
     * @param int    $state The state of the handler
     * @param int    $pos The position in the document
     * @param Doku_Handler    $handler The handler
     * @return array Data for the renderer
     */
    public function handle($match, $state, $pos, Doku_Handler &$handler){
        $data = array($state, $match);

        return $data;
    }

    /**
     * Render xhtml output or metadata
     *
     * @param string         $mode      Renderer mode (supported modes: xhtml)
     * @param Doku_Renderer  $renderer  The renderer
     * @param array          $data      The data from the handler() function
     * @return bool If rendering was successful.
     */
    public function render($mode, Doku_Renderer &$renderer, $data) {
        if($mode != 'xhtml') return false;

        if ($data[0] == DOKU_LEXER_UNMATCHED)
        {
            $result = $this->runDrools($data[1]);
            $renderer->doc .= nl2br($result);
        }

        return true;
    }

    public function runDrools($rules)
    {
        escapeshellarg($rules);
        $engine = $this->getConf('drools_engine_cmd');
        $sandbox = $this->getConf('sandbox_cmd');
        $cmd = $sandbox.' '.$engine.' '.escapeshellarg($rules);

        return shell_exec($cmd);
    }
}

// vim:ts=4:sw=4:et:
